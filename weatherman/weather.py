'''
Get the remote weather
'''
import os
import re
import json
from datetime import datetime
import requests
from .config import TENDAY_URL, ICON_DIR, CACHE_PATH, CACHE_TTL


RE_CHANCE_OF_RAIN = re.compile(r'.*of rain (\d+)%')


def chance_of_rain(s):
    m = RE_CHANCE_OF_RAIN.match(s.lower())
    if m:
        return int(m.group(1))
    return None


def timedelta_minutes(td):
    days = td.days
    seconds = td.seconds
    return ((days * 24 * 3600) + seconds) / 60


def load_cache():
    if not os.path.exists(CACHE_PATH):
        last_time = datetime(1970, 1, 1)
        cache = {'last_request_time': last_time.isoformat()}
    else:
        with open(CACHE_PATH) as f:
            cache = json.load(f)
    try:
        last_time = datetime.strptime(cache['last_request_time'],
                                      '%Y-%m-%dT%H:%M:%S.%f')
    except:
        last_time = datetime.strptime(cache['last_request_time'],
                                      '%Y-%m-%dT%H:%M:%S')
    if timedelta_minutes(datetime.now() - last_time) >= CACHE_TTL:
        return None
    else:
        return cache['last_request']


def save_cache(data):
    cache = {'last_request_time': datetime.now().isoformat()}
    cache['last_request'] = data
    with open(CACHE_PATH, 'w') as f:
        json.dump(cache, f, indent=4)


def get_weather_data():
    cache = load_cache()
    if cache is not None:
        return cache
    response = requests.get(TENDAY_URL)
    data = json.loads(response.text)
    save_cache(data)
    return data


def download_if_none(name, url):
    path = os.path.join(ICON_DIR, name + '.gif')
    if not os.path.exists(path):
        img_data = requests.get(url).content
        with open(path, 'wb') as f:
            f.write(img_data)


def download_icons(data):
    forecast = data['forecast']['txt_forecast']['forecastday']
    for day in forecast:
        icon_name = day['icon']
        icon_url = day['icon_url']
        download_if_none(icon_name, icon_url)


def print_week(data):
    forecast = data['forecast']['txt_forecast']['forecastday']
    for day in forecast:
        chance = chance_of_rain(day['fcttext'])
        rain = ''
        if chance is not None:
            rain = ' RAIN={}%'.format(chance)
        print('{day[title]}{rain}: {day[fcttext]}'.format(day=day, rain=rain))


def request_and_print():
    data = get_weather_data()
    download_icons(data)
    print_week(data)
    return data
