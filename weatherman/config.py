import os
from confutil import Config

CONFIG = Config('weatherman')
TENDAY_URL = ('http://api.wunderground.com/api/{}/forecast10day/q/{}/{}.json'
              .format(CONFIG['API_KEY'], CONFIG['STATE'], CONFIG['CITY']))
CONFIG_DIR = os.path.expanduser('~/.config/weatherman')
ICON_DIR = os.path.join(CONFIG_DIR, 'icons')
if not os.path.exists(ICON_DIR):
    os.makedirs(ICON_DIR)
CACHE_PATH = os.path.join(CONFIG_DIR, 'cache.json')
CACHE_TTL = int(CONFIG.get('CACHE_TTL', '20'))
