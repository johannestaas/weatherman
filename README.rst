weatherman
==========

Weather forecast

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Simply run it::

    $ weatherman

Use --help/-h to view info on the arguments::

    $ weatherman --help

Release Notes
-------------

:0.0.1:
    Project created
